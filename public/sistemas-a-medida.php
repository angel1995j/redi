<?php
$title = "Desarrollo de sistemas a medida";
include_once 'includes/head.php'?>
    <body id="top" class="has-header-search">

       <?php
include_once 'includes/header-page.html';
include_once 'includes/menu.php';

?>
       <!--page title start-->
        <section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Servicios</h2>
                        <ol class="breadcrumb">
                            <li><a href="#">Inicio</a></li>
                            <li >Servicios</li>
                            <li class="active" >Sistemas a Medida</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->

        <section class=" section-padding lighten-4">
            <div class="container">
              <div class="row">
                <div class="col-md-7">
                	<h2 class="font-40 mb-30 ">DESARROLLO DE SISTEMAS A LA MEDIDA</h2>
                	<p class="text-justify">Buscamos las necesidades y problemas que atacan a las empresas diariamente y las transformamos en sistemas de información, en Markdevs hemos realizado sistemas para múltiples giros de negocio, por lo que nuestros desarrollos se adaptan a casi cualquier necesidad.
                  Somos tus aliados en el camino a la digitalización de tu empresa.</p>

                	<ul class="list-icon mb-30">
                		<li><i class="material-icons">&#xE876;</i> Desarrollo efectivo.</li>
                		<li><i class="material-icons">&#xE876;</i> Usamos las tecnologías más nuevas y estables.</li>
                		<li><i class="material-icons">&#xE876;</i> Nos adaptamos 100% a tus necesidades.</li>
                	</ul>

                    <a href="contacto" class="m-auto btn btn-lg text-capitalize waves-effect waves-light markdevs">
                    <i class="material-icons left">headset_mic</i>
                    Contactar un agente</a>                        
                </div><!-- /.col-md-7 -->

                <div class="col-md-5 mt-sm-30">
					         <img src="assets/img/services/sistemas.jpeg" alt="sistemas a medida" class="img-responsive">
                </div><!-- /.col-md-5 -->
              </div><!-- /.row -->
            </div><!-- /.container -->
        </section>


<?php
include_once 'includes/footer.html';
include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>



<?php
include_once 'includes/footer.html';
include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>
