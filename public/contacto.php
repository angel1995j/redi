<?php
$title = "Contacto - Agencia de desarrollo web";
include_once 'includes/head.php'?>
    <body id="top" class="has-header-search">

       <?php
include_once 'includes/header-page.html';
include_once 'includes/menu.php';
//include_once 'includes/hero.html';

?>

       
            <!-- contact-form-section -->
        <section class="section-padding">
          
          <div class="container">

            <div class="text-center mb-250">
                <h2 class="section-title text-uppercase">Contacto</h2>
                <p class="section-sub">¡Déjanos tu mensaje!, alguien de nuestro equipo se estará poniendo en contacto contigo para poderte darte la atención que mereces.</p>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <form name="contact-form" id="contactForm" action="includes/sendemail.php" method="POST">

                      <div class="row">
                        <div class="col-md-6">
                          <div class="input-field">
                            <input type="text" name="name" class="validate" id="name" style="height: 5em;">
                            <label for="name">NOMBRE</label>
                          </div>

                        </div><!-- /.col-md-6 -->

                        <div class="col-md-6">
                          <div class="input-field">
                            <label class="sr-only" for="email">CORREO ELÉCTRONICO</label>
                            <input id="email" type="email" name="email" class="validate" style="height: 5em;">
                            <label for="email" data-error="wrong" data-success="right">CORREO ELÉCTRONICO</label>
                          </div>
                        </div><!-- /.col-md-6 -->
                      </div><!-- /.row -->

                      <div class="row">
                        <div class="col-md-6">
                          <div class="input-field">
                            <input id="phone" type="tel" name="phone" class="validate" style="height: 5em;">
                            <label for="phone">NÚMERO TÉLEFONICO</label>
                          </div>
                        </div><!-- /.col-md-6 -->

                        <div class="col-md-6">
                          <div class="input-field">
                            <input id="message" type="text" name="message" class="validate" style="height: 5em;">
                            <label for="message">TÚ MENSAJE</label>
                          </div>
                        </div><!-- /.col-md-6 -->
                      </div><!-- /.row -->
                       
                     
                      <button type="submit" name="submit" class="waves-effect waves-light btn submit-button black mt-30 mb-sm-30">ENVIAR MENSAJE</button>

                    </form>
                </div><!-- /.col-md-8 -->

                
            </div><!-- /.row -->
          </div>
        </section>
        <!-- contact-form-section End -->



        <?php
include_once 'includes/footer.html';
include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>