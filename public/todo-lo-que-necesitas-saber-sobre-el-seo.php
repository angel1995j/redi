
<?php 
    $title = "Todo lo que necesitas saber sobre SEO";
    $path = dirname(__DIR__);
    include_once 'includes/head.php';?>
 <body id="top" class="has-header-search">

       <?php
        include_once 'includes/header-page.html';
        include_once 'includes/menu.php';
        ?>

        <!--page title start-->
        <section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Nuestro Articulo</h2>
                        <ol class="breadcrumb">
                            <li><a href="#">Inicio</a></li>
                            <li><a href="#">Blog</a></li>
                         
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->
        

        <!-- blog section start -->
        <section class="blog-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                      <div class="posts-content single-post">

                        <article class="post-wrapper">

                          <header class="entry-header-wrapper clearfix">

                            <div class="author-thumb waves-effect waves-light">
                              <a href="#"><img src="assets/img/blog/author.jpg" alt="autor"></a>                
                            </div>
                            
                            <div class="entry-header">
                              <h2 class="entry-title">TODO LO QUE NECESITAS SABER SOBRE EL SEO</h2>

                              <div class="entry-meta">
                                  <ul class="list-inline">
                                      <li>
                                          <i class="fa fa-user"></i><a href="#">ADMIN</a>
                                      </li>

                                      <li>
                                          <i class="fa fa-clock-o"></i><a href="#">Ene 21, 2019</a>
                                      </li>

                                      <li>
                                          <i class="fa fa-heart-o"></i><a href="#"><span>314</span></a>
                                      </li>

                                      <li>
                                          <i class="fa fa-comment-o"></i><a href="#">0</a>
                                      </li>
                                  </ul>
                              </div><!-- .entry-meta -->
                            </div>

                          </header><!-- /.entry-header-wrapper -->

                          <div class="entry-header-wrapper clearfix">
                            <img src="assets/img/blog/blog2.jpg" class="img-responsive" alt="seo" >
                          </div><!-- .post-thumb -->


                          <div class="entry-content">


                            <p></p>

                            <p class="text-justify">Bienvenido de nuevo al Blog, hoy vamos a hablar de algo que diariamente nos preguntan en Markdevs y que 
                            obviamente esta en nuestros servicios, vamos a tratar de despejar a muy agroso modo las dudas más comunes,
                            sin más comencemos explicando un poco al respecto y bien a todo esto</p>

                            <h2>¿Que es el SEO?</h2>

                            <p class="text-justify">Explicado coloquialmente, el SEO es un conjunto de estrategias, técnicas y/o métodos que te llevan a tener una web optimizada para que los motores de búsqueda (Google, Yahoo, Bing) decidan indexar tu contenido y esto se logra mediante dos principales métodos:</p>

                            <p class="text-justify">SEO ONPAGE: El SEO OnPage se refiere en términos generales al conjunto de métodos y técnicas que se utilizan a nivel del contenido para mejorar el posicionamiento de esta, esto influye muy directamente porque supongamos que Juan Perez busca “Tienda de mascotas en México” , si tu has colocado esa palabra probablemente Juan Perez va encontarr tu sitio, obviamente mientras el contenido sea de cálidad y exista una cantidad importante el resultado será mejor, algunos datos que debes tener en cuenta para crear buen contenido:</p>
                           
                <div class="row">
                        <div class="mt-30"></div>
                    
                        <table class="table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Que sea 100% original</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Que tegna 350 palabras o más</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Al menos una etiqueta H1</td>
                            </tr>
                            <tr>
                                <th scope="row">4</th>
                                <td>Al menos una eqtiqueta H2</td>
                            </tr>
                            <tr>
                                <th scope="row">5</th>
                                <td>Tu palabra clave mencionada orgánicamente en el articulo</td>
                            </tr>
                            <tr>
                                <th scope="row">6</th>
                                <td>Natutalidad de adaptación de la palabra clave</td>
                            </tr>
                            <tr>
                                <th scope="row">7</th>
                                <td>Uso de texto bold</td>
                            </tr>
                            <tr>
                                <th scope="row">8</th>
                                <td>Etiqueta Meta descripción</td>
                            </tr>
                            <tr>
                                <th scope="row">9</th>
                                <td>Etiqueta Title</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.row -->


                            <p class="text-justify">Habría algunas cosas adicionales que habría que cuidar pero la recomendación siempre es crear contenido de calidad, por ahi alguinas personas que saben de SEO comentan que “el contenido es el rey”, yo sinceramente estoy de acuerdo con esa información , pero ahi personas que quiza no lo esten.</p>

                            <h2>SEO OFFPAGE</h2>
                            <p class="text-justify">El SEO OFF Page es un conjunto de tecnicas que llevan a la mejora de tu sitio web, a nivel de experiencia de usuario con el fin que sea aceptado más facilmente por los prinicipales buscadores, algunas de las cosas que se deberian considerar en el SEO Off page van solamente muy relacionadas a la usabilidad de la misma como experiencia para el usuario.</p>

                            <p class="text-justify">Idealmente si el sitio cuenta con muy buenas practicas de optimizacion el problem deberia ser muy menor pero lamentablemente es algo que no siempre se logra.</p>
                           
                            <p class="text-justify">Si acaso llegas a tener dudas , por favor contactanos y te atendemos sin costo , pafra nosotros es importante saber tu opinión.</p>

                            
                          </div><!-- .entry-content -->


                          <footer class="entry-footer">
                            <div class="post-tags">
                              <span class="tags-links">
                                <i class="fa fa-tags"></i><a href="#">Tecnología,,</a> <a href="#" rel="tag"></a>
                              </span>
                            </div> <!-- .post-tags -->

                            <ul class="list-inline right share-post">
                                <li><a href="#"><i class="fa fa-facebook"></i> <span>Compartir</span></a>
                                </li>                                
                                </li>
                                <li><a href="#"><i class="fa fa-google-plus"></i> <span>Más</span></a>
                                </li>
                            </ul>
                          </footer>

                        </article><!-- /.post-wrapper -->

                        <nav class="single-post-navigation" role="navigation">
                          <div class="row">
                            <!-- Previous Post -->
                            <div class="col-xs-6">
                              <div class="previous-post-link">
                                <a class="waves-effect waves-light" href="importancia-del-diseno-responsivo.php"><i class="fa fa-long-arrow-left"></i>Leer publicación anterior</a>
                              </div>
                            </div>

                            <!-- Next Post -->
                            <div class="col-xs-6">
                              <div class="next-post-link">
                                <a class="waves-effect waves-light" href="que-pasa-con-los-servidores-en-nube.php">Leer siguiente publicación<i class="fa fa-long-arrow-right"></i></a>
                              </div>
                            </div>
                          </div> <!-- .row -->
                        </nav>

                      </div><!-- /.posts-content -->
                    </div><!-- /.col-md-12 -->

                  </div><!-- /.row -->
            </div><!-- /.container -->
        </section>
        <!-- blog section end -->

        <?php
        include_once 'includes/footer.html';
        include_once 'includes/preloader.html';
        include_once 'includes/scripts.html';
         ?>
