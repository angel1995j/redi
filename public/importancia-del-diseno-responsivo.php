
<?php 
    $title = "Importancia del diseño responsivo";
    $path = dirname(__DIR__);
    include_once 'includes/head.php';?>
 <body id="top" class="has-header-search">

       <?php
        include_once 'includes/header-page.html';
        include_once 'includes/menu.php';
        ?>

        <!--page title start-->
        <section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Nuestro Articulo</h2>
                        <ol class="breadcrumb">
                            <li><a href="#">Inicio</a></li>
                            <li><a href="#">Blog</a></li>
                         
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->
        

        <!-- blog section start -->
        <section class="blog-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                      <div class="posts-content single-post">

                        <article class="post-wrapper">

                          <header class="entry-header-wrapper clearfix">

                            <div class="author-thumb waves-effect waves-light">
                              <a href="#"><img src="assets/img/blog/author.jpg" alt="autor"></a>                
                            </div>
                            
                            <div class="entry-header">
                              <h2 class="entry-title">IMPORTANCIA DEL DISEÑO RESPONSIVO</h2>

                              <div class="entry-meta">
                                  <ul class="list-inline">
                                      <li>
                                          <i class="material-icons md-medio-15">supervised_user_circle</i><a href="#">ADMIN</a>
                                      </li>

                                      <li>
                                          <i class="material-icons md-18">access_time</i><a href="#">Jun 25, 2019</a>
                                      </li>

                                      <li>
                                          <i class="material-icons md-18">favorite_border</i><a href="#"><span>1</span></a>
                                      </li>

                                      <li>
                                          <i class="material-icons md-18">chat_bubble_outline</i><a href="#">0</a>
                                      </li>
                                  </ul>
                              </div><!-- .entry-meta -->
                            </div>

                          </header><!-- /.entry-header-wrapper -->

                          <div class="entry-header-wrapper clearfix">
                            <img src="assets/img/blog/blog1.jpg" class="img-responsive" alt="imagen" >
                          </div><!-- .post-thumb -->


                          <div class="entry-content">
                            <p class="text-justify">El diseño web responsivo se define como un diseño adaptado que permite visualizarse de una forma correcta y 
                            adaptable del sitio web. Hoy en día un gran porcentaje de los usuarios utiliza un dispositivo móvil para navegar
                            por Internet por eso es importante que tu sitio web pueda ser visualizado desde todos los dispositivos como
                            computadoras comunes, dispositivos móviles y tabletas.</p>

                            <h2>¿Qué importancia tiene un diseño Responsivo?</h2>

                            <p class="text-justify">Hoy en día es de suma importancia que tu sitio web sea responsivo, se adapte a todos los equipos y usuarios, 
                            sin perder claridad de los detalles.</p>

                            <p class="text-justify">El hecho de no tener un sitio responsivo puede generar problemas en las vistas de los usuarios y afectarnos en 
                            cuanto a la promoción que le damos a nuestro sitio o al marketing online que se le dé.  Por otra parte, el perder 
                            visitas lleva a generar conflictos de Posicionamiento en buscadores de Google ya que este adapto como principal 
                            requisito para tomar en cuenta tu sitio web que este cuente con un diseño responsivo.</p>

                            <h2>Ventajas</h2>
                            <p class="text-justify">Contar con este tipo de diseño para nuestra web es sinónimo de facilidad de visualización, calidad y nos permite 
                            contar con las siguientes ventajas:</p>

                            
                           
                           
                            

                <div class="row">
                        <div class="mt-30"></div>
                    
                        <table class="table">
                            <thead>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>El diseño es soportado por todos los navegadores usados actualmente.</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Ahorro en tiempo y costo de diseño: se hace un sólo diseño para todos los dispositivos.</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td> Tu sitio se puede visualizar desde una misma dirección de enlace.</td>
                            </tr>
                            <tr>
                                <th scope="row">4</th>
                                <td> El mismo diseño web es útil para todos los dispositivos esto te ahorra tiempo y costo.</td>
                            </tr>
                            <tr>
                                <th scope="row">5</th>
                                <td>El web responsivo tiene un mayor posicionamiento y está recomendada por Google.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.row -->

                          </div><!-- .entry-content -->


                          <footer class="entry-footer">
                            <div class="post-tags">
                              <span class="tags-links">
                                <i class="material-icons md-18">loyalty</i><a href="#">Tecnología,,</a> <a href="#" rel="tag"></a>
                              </span>
                            </div> <!-- .post-tags -->

                            <ul class="list-inline right share-post">
                                <li><a href="#"><i class="material-icons md-18">share</i> <span>Compartir</span></a>
                                </li>                                
                                </li>
                                <li><a href="#"><i class="material-icons md-18">plus_one</i> <span>Más</span></a>
                                </li>
                            </ul>
                          </footer>

                        </article><!-- /.post-wrapper -->

                        <nav class="single-post-navigation" role="navigation">
                          <div class="row">
                            <!-- Previous Post -->
                            <div class="col-xs-6">
                              <div class="previous-post-link">
                                <a class="waves-effect waves-light" href="que-pasa-con-los-servidores-en-nube.php"><i class="fa fa-long-arrow-left"></i>Leer publicación anterior</a>
                              </div>
                            </div>

                            <!-- Next Post -->
                            <div class="col-xs-6">
                              <div class="next-post-link">
                                <a class="waves-effect waves-light" href="todo-lo-que-necesitas-saber-sobre-el-seo.php">Leer publicación siguiente <i class="fa fa-long-arrow-right"></i></a>
                              </div>
                            </div>
                          </div> <!-- .row -->
                        </nav>

                      </div><!-- /.posts-content -->
                    </div><!-- /.col-md-12 -->

                  </div><!-- /.row -->
            </div><!-- /.container -->
        </section>
        <!-- blog section end -->

        <?php
        include_once 'includes/footer.html';
        include_once 'includes/preloader.html';
        include_once 'includes/scripts.html';
         ?>
