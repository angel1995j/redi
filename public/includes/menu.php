<?php

$url = $_SERVER["REQUEST_URI"];
?>

<div id="matrox-menu" class="menuzord">

    <!--logo start-->
    <a href="/redi/public/" class="logo-brand">
        <img class="retina" src="assets/img/logo.png" alt="MarkDevs" />
    </a>
    <!--logo end-->

    <!--mega menu start-->
    <ul class="menuzord-menu pull-right">
        <li><a href="/redi/public/">Inicio</a>
        </li>

        <li><a href="/redi/public/nosotros">Nosotros</a>

        </li>

        <li><a class=" page-scroll
                      tt-animate btt"
                      <?php $services = ($url == "/redi/public/") ? "#servicios" : "https://markdevs.com/#servicios"; ?>
                href="<?= $services ?>">Servicios</a>
                <ul class="dropdown">
                    <li><a href="/redi/public/sistemas-a-medida">Sitemas a medida</a></li>
                    <li><a href="/redi/public/paginas-web">Páginas Web</a></li>
                    <li><a href="/redi/public/comercio-electronico">Comercio Electrónico</a></li>
                    <li><a href="/redi/public/marketing-digital">Marketing Digital</a></li>
                </ul>
        </li>

        <li><a href="/redi/public/proyectos">Proyectos</a>
        </li>

        <li><a href="/redi/public/blog">Blog</a>
        </li>

        <li><a href="/redi/public/contacto">Contacto</a>
        </li>


    </ul>
    <!--mega menu end-->

</div>
</div>
</div>
</header>
<!--header end-->