<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
    <script rel="prefetch" async src="https://www.googletagmanager.com/gtag/js?id=UA-137180009-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-137180009-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Agencia de desarrollo web">
    <meta name="author" content="Markdevs">

    <title><?php
echo "Markdevs - ";
echo isset($title) ? $title : 'Agencia de desarrollo web';
?></title>

    <!--  favicon -->
    <link rel="shortcut icon" href="assets/img/ico/favicon.png">
    <!--  apple-touch-icon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png">

    <!-- <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'> -->
    <link href='assets/fonts/raleway.css' rel='stylesheet' type='text/css'>
    <link href='assets/fonts/iconfont/material-icons.css' rel='stylesheet' type='text/css'>
    <!-- Material Icons CSS -->
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <!-- FontAwesome CSS -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet"> -->
    <!-- magnific-popup -->
    <!-- <link href="assets/magnific-popup/magnific-popup.css" rel="stylesheet"> -->
    <!-- flexslider -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.4.0/flexslider.css" rel="stylesheet"> -->
    <!-- animated-headline -->
    <link href="assets/css/animated-headline.css" rel="stylesheet">
    <!-- materialize -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.95.1/css/materialize.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
      <!-- FONTastic-->
<link href="assets/css/fontastic.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->
    <!-- shortcodes -->
    <link href="assets/css/shortcodes/shortcodes.css" rel="stylesheet">

 <!-- Style CSS -->
    <link href="style.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>