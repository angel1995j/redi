
<?php 
    $title = "Nosotros";
    include_once 'includes/head.php';?>
 <body id="top" class="has-header-search">

       <?php
        include_once 'includes/header-page.html';
        include_once 'includes/menu.php';

        ?>

        <!--page title start-->
        <section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Acerca de Nosotros</h2>
                        <ol class="breadcrumb">
                            <li><a href="#">Inicio</a></li>
                            <li class="active">Nosotros</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->
        

        <section class="section-padding">
            <div class="container">

              <h2 class="font-30 text-medium mb-30" style="color: #d7a531;">Somos Markdevs</h2>
              <div class="row">
                <div class="col-md-7">
                  
                  <p class="text-justify">Somos una empresa dedicada al desarrollo de Sistemas a la Medida, Sitios Web Informativos y de Comercio Electrónico, ofreciendo soluciones a medida para todo tipo de organizaciones, así como también a emprendedores, sin distinguir sectores ni envergadura. MarkDevs nace en la ciudad de Morelia – México, con la intención de brindar a las empresas soluciones para sus problemas administrativos, financieros y operativos, a partir de la digitalización tanto de sus procesos como de sus proyectos, brindando mayores oportunidades en lo que respecta a comercio electrónico y sitios informativos.</p>

                  <p class="text-justify">En MarkDevs nos destacamos por alcanzar siempre resultados innovadores y con elevados niveles de satisfacción por parte de nuestros clientes. A partir de esto, consolidamos alianzas estratégicas que nos permitieron ampliar nuestro mercado, aterrizando en Argentina a partir del año 2018, incorporando, además, a nuestra cartera de servicios productos altamente innovadores que brindan soluciones puntuales a problemáticas históricas, alcanzando resultados destacados de forma altamente eficiente.</p>
                </div><!-- /.col-md-7 -->

                <div class="col-md-5">
                  
                    
                        <img src="assets/img/blog/nosotros.jpeg" alt="nosotros" class="img-responsive">
  
                  </div><!-- /.gallery-thumb -->
                </div><!-- /.col-md-5 -->
              </div><!-- /.row -->
            </div><!-- /.container -->
        </section>


        <section class=" bg-fixed ptb-110 overlay light-9" data-stellar-background-ratio="0.5">
            <div class="container">

              <div class="text-center mb-80">
                  <h2 class="section-title text-uppercase">soluciones integrales</h2>
                  <p class="section-sub">En MarkDevs trabajamos para ofrecer soluciones que se adaptan a todos los segmentos de mercado a partir del desarrollo de Sistemas Integrales que tejen redes hacia todos los 
                      aspectos estratégicos y de impacto, necesarios para alcanzar resultados destacados en la arena del mercado en el cual se desenvuelven tanto las grandes, medianas, pequeñas empresas y todo tipo de emprendedores.
                      ¡Estamos ansiosos por obtener tus consultas y comentarios!</p>
              </div>

            </div><!-- /.container -->
        </section>

        <hr>
        

            <?php
        include_once 'includes/footer.html';
        include_once 'includes/preloader.html';
        include_once 'includes/scripts.html';
         ?>