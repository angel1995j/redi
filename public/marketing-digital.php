<?php
$title = "Marketing Digital";
include_once 'includes/head.php'?>
    <body id="top" class="has-header-search">

       <?php
include_once 'includes/header-page.html';
include_once 'includes/menu.php';

?>
        <!--page title start-->
        <section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Servicios</h2>
                        <ol class="breadcrumb">
                            <li><a href="#">Inicio</a></li>
                            <li >Servicios</li>
                            <li class="active" >Marketing Digital</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->
        <section class=" section-padding lighten-4">
            <div class="container">
              <div class="row">
                <div class="col-md-7">
                	<h2 class="font-40 mb-30 ">MARKETING DIGITAL</h2>
                	<p class="text-justify">Manejamos estrategias digitales probadas y desarrolladas en el mercado, calculamos todas nuestras campañas para que tengas un retorno de inversión garantizado.</p>

                	<ul class="list-icon mb-30">
                		<li><i class="material-icons">&#xE876;</i> Desarrollo de campañas con cualquier presupuesto.</li>
                		<li><i class="material-icons">&#xE876;</i> Estádisticas y entrega de reportes.</li>
                		<li><i class="material-icons">&#xE876;</i> Buscamos el retorno de inversión.</li>
                        <li><i class="material-icons">&#xE876;</i> Realizamos planeaciones previas.</li>
                	</ul>
                	<a href="contacto" class="m-auto btn btn-lg text-capitalize waves-effect waves-light markdevs">
                    <i class="material-icons left">headset_mic</i>
                    Contactar un agente</a>
                </div><!-- /.col-md-7 -->

                <div class="col-md-5 mt-sm-30">
					         <img src="assets/img/services/marketing.jpeg" alt="marketing" class="img-responsive">
                </div><!-- /.col-md-5 -->
              </div><!-- /.row -->
            </div><!-- /.container -->
        </section>


<?php
include_once 'includes/footer.html';
include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>



<?php
include_once 'includes/footer.html';
include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>
