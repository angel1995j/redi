<?php
$title = "Desarrollo de comercios electrónicos";
include_once 'includes/head.php'?>
    <body id="top" class="has-header-search">

       <?php
include_once 'includes/header-page.html';
include_once 'includes/menu.php';

?>


        <!--page title start-->
        <section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Servicios</h2>
                        <ol class="breadcrumb">
                            <li><a href="#">Inicio</a></li>
                            <li >Servicios</li>
                            <li class="active" >Comercio Electrónico</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->

        <section class=" section-padding lighten-4">
            <div class="container">
              <div class="row">
                <div class="col-md-7">
                	<h2 class="font-40 mb-30 ">DESARROLLO DE COMERCIO ELECTRÓNICO</h2>
                	<p class="text-justify">El comercio electrónico ha tenido un importante crecimiento en los últimos años, es momento de que tu negocio pueda vender en linea, en Markdevs te ayudamos con todo el proceso.</p>

                	<ul class="list-icon mb-30">
                        <li><i class="material-icons">&#xE876;</i> Pasarelas de pago.</li>
                        <li><i class="material-icons">&#xE876;</i> Estadisicas y gestión de pedidos.</li>
                		<li><i class="material-icons">&#xE876;</i> Desarrollo efectivo.</li>
                		<li><i class="material-icons">&#xE876;</i> Usamos las tecnologías más nuevas y estables.</li>
                		<li><i class="material-icons">&#xE876;</i> Nos adaptamos 100% a tus necesidades.</li>
                	</ul>
                	<a href="contacto" class="m-auto btn btn-lg text-capitalize waves-effect waves-light markdevs">
                    <i class="material-icons left">headset_mic</i>
                    Contactar un agente</a>
                        <a target="_blank" href="redi/public/cotizar" class="btn btn-lg waves-effect waves-light text-bold markdevs-b">
                     <i class="material-icons left">attach_money</i>
                     Cotizar en Linea</a>
                </div><!-- /.col-md-7 -->

                <div class="col-md-5 mt-sm-30">
					         <img src="assets/img/services/comercio.jpeg" alt="Comercio Electrónico" class="img-responsive">
                </div><!-- /.col-md-5 -->
              </div><!-- /.row -->
            </div><!-- /.container -->
        </section>


<?php
include_once 'includes/footer.html';
include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>



<?php
include_once 'includes/footer.html';
include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>
