
<?php 
$title = "Blog - Agencia de desarrollo web";
include_once 'includes/head.php'?>
<body id="top" class="has-header-search">

   <?php
    include_once 'includes/header-page.html';
    include_once 'includes/menu.php';

    ?>
        <!--page title start-->
        <section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Nuetro Blog</h2>
                        <ol class="breadcrumb">
                            <li><a href="#">Inicio</a></li>
                            <li><a href="#">Blog</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->


        <!-- Grid News -->
        <section class="section-padding grid-news-hover grid-blog">
            <div class="container">

              <div class="row">
                <div id="blogGrid">

                  <div class="col-xs-12 col-sm-6 col-md-3 blog-grid-item">
                      <article class="post-wrapper">

                        <div class="thumb-wrapper waves-effect waves-block waves-light">
                          <a href="importancia-del-diseno-responsivo.php"><img src="assets/img/blog/blog1.jpg" class="img-responsive" alt="blog" ></a>
                          <div class="post-date">
                              25<span>Marzo</span>
                          </div>
                        </div><!-- .post-thumb -->

                        <div class="blog-content">

                          <div class="hover-overlay light-blue"></div>

                          <header class="entry-header-wrapper">
                            <div class="entry-header">
                              <h2 class="entry-title"><a href="importancia-del-diseno-responsivo.php">IMPORTANCIA DEL DISEÑO RESPONSIVO</a></h2>

                              <div class="entry-meta">
                                  <ul class="list-inline">
                                      <li>
                                          By <a href="#">Admin</a>
                                      </li>
                                      <li>
                                          In <a href="#">Technology</a>
                                      </li>
                                  </ul>
                              </div><!-- .entry-meta -->
                            </div><!-- /.entry-header -->
                          </header><!-- /.entry-header-wrapper -->

                          <div class="entry-content">
                            <p>El diseño web responsivo se define como un diseño adaptado que permite visualizarse de una forma correcta y 
                                adaptable del sitio web.</p>
                          </div><!-- .entry-content -->

                        </div><!-- /.blog-content -->

                      </article><!-- /.post-wrapper -->
                  </div><!-- /.col-md-3 -->

                  <div class="col-xs-12 col-sm-6 col-md-3 blog-grid-item">
                      <article class="post-wrapper">

                        <div class="thumb-wrapper waves-effect waves-block waves-light">
                          <a href="todo-lo-que-necesitas-saber-sobre-el-seo.php"><img src="assets/img/blog/blog2.jpg" class="img-responsive" alt="blog" ></a>
                          <div class="post-date">
                              21<span>Enero</span>
                          </div>
                        </div><!-- .post-thumb -->

                        <div class="blog-content">

                          <div class="hover-overlay light-blue"></div>

                          <header class="entry-header-wrapper">
                            <div class="entry-header">
                              <h2 class="entry-title"><a href="todo-lo-que-necesitas-saber-sobre-el-seo.php">TODO LO QUE NECESITAS SABER SOBRE EL SEO</a></h2>

                              <div class="entry-meta">
                                  <ul class="list-inline">
                                      <li>
                                          By <a href="#">Admin</a>
                                      </li>
                                      <li>
                                          In <a href="#">Technology</a>
                                      </li>
                                  </ul>
                              </div><!-- .entry-meta -->
                            </div><!-- /.entry-header -->
                          </header><!-- /.entry-header-wrapper -->

                          <div class="entry-content">
                            <p>Explicado coloquialmente, el SEO es un conjunto de estrategias, técnicas y/o métodos que 
                                te llevan a tener una web optimizada para que los motores de búsqueda </p>
                          </div><!-- .entry-content -->

                        </div><!-- /.blog-content -->

                      </article><!-- /.post-wrapper -->
                  </div><!-- /.col-md-3 -->

                  <div class="col-xs-12 col-sm-6 col-md-3 blog-grid-item">
                      <article class="post-wrapper">

                        <div class="thumb-wrapper waves-effect waves-block waves-light">
                          <a href="que-pasa-con-los-servidores-en-nube.php"><img src="assets/img/blog/blog3.jpg" class="img-responsive" alt="blog" ></a>
                          <div class="post-date">
                              10<span>Dic</span>
                          </div>
                        </div><!-- .post-thumb -->

                        <div class="blog-content">

                          <div class="hover-overlay light-blue"></div>

                          <header class="entry-header-wrapper">
                            <div class="entry-header">
                              <h2 class="entry-title"><a href="que-pasa-con-los-servidores-en-nube.php">¿QUE PASA CON LOS SERVIDORES EN NUBE?</a></h2>

                              <div class="entry-meta">
                                  <ul class="list-inline">
                                      <li>
                                          By <a href="#">Admin</a>
                                      </li>
                                      <li>
                                          In <a href="#">Technology</a>
                                      </li>
                                  </ul>
                              </div><!-- .entry-meta -->
                            </div><!-- /.entry-header -->
                          </header><!-- /.entry-header-wrapper -->

                          <div class="entry-content">
                            <p>El tema de los servidores en nube definitivamente es el top del momento, 
                                ¿Pero porque causan tanto revuelo?, ¿que los hace tan geniales como dicen? </p>
                          </div><!-- .entry-content -->

                        </div><!-- /.blog-content -->

                      </article><!-- /.post-wrapper -->
                  </div><!-- /.col-md-3 -->

                  
                
                  

                </div><!-- /#blogGrid -->

              </div><!-- /.row -->

              <ul class="pagination post-pagination text-center mt-50">
                <li><a href="#." class="waves-effect waves-light"><i class="fa fa-angle-left"></i></a></li>
                <li><span class="current waves-effect waves-light">1</span></li>
                <!--<li><a href="#." class="waves-effect waves-light">2</a></li> -->
                <li><a href="#." class="waves-effect waves-light"><i class="fa fa-angle-right"></i></a></li>
              </ul>


            </div><!-- /.container -->
        </section>
        <!-- Grid News End -->

        <?php
        include_once 'includes/footer.html';
        include_once 'includes/preloader.html';
        include_once 'includes/scripts.html';
         ?>