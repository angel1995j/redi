<?php
$title = "Agencia de desarrollo web";
include_once 'includes/head.php'?>
    <body id="top" class="has-header-search">

       <?php
include_once 'includes/header.html';
include_once 'includes/menu.php';
include_once 'includes/hero.html';

?>


<section class="section-padding">
    <div class="container">
        <?php include_once 'includes/servicesHome.html';?>

        <div class="text-center mt-80 mb-80">
            <h2 class="section-title text-uppercase text-bold">
            ¡SOMOS TU ALIADO ESTRATÉGICO AL ÉXITO!
            </h2>
            <p class="section-sub">
            En Markdevs nos encantan los retos,
            no importa si eres una pequeña startup o una gran empresa,
            estamos preparados para tu desarrollo, contamos con un amplio
            catálogo de proyectos concluidos.
            </p>

        </div>

        <div class="promo-box dark-bg mb-50">
                <div class="promo-info">
                    <h2 class="white-text text-uppercase text-xs-center text-bold no-margin">Conoce nuestra herramienta para <span class="brand-color">COTIZAR</span> en linea</h2>
                </div>
                <div class="promo-btn text-center">
                     <a target="_blank" href="/redi/public/cotizar/" class="btn btn-lg waves-effect waves-light text-bold markdevs-b">
                     <i class="material-icons left">attach_money</i>
                     ¡Cotizar Ahora!</a>
                </div>
        </div>

        <div class="text-center mt-80 mb-80">
            <h2 class="section-title text-uppercase text-bold">
            ¿CÓMO SE TRANSFORMA TU IDEA A UN PRODUCTO?
            </h2>
        </div>

      <div class="vertical-tab">
                <div class="row">
                  <div class="col-sm-3">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-stacked">
                          <li class="active">
                              <a href="#tab-5" class="waves-effect waves-light"  data-toggle="tab">
                              Conocemos tu idea</a></li>
                          <li ><a href="#tab-6" class="waves-effect waves-light"  data-toggle="tab">
                              Platificamos el concepto</a></li>
                          <li ><a href="#tab-7" class="waves-effect waves-light" data-toggle="tab">
                              Desarrollamos</a></li>
                          <li ><a href="#tab-8" class="waves-effect waves-light" data-toggle="tab">
                              ¡Tenemos listo tu producto!</a></li>
                        </ul>                      
                  </div><!-- /.col-md-3 -->

                  <div class="col-sm-9">
                        <!-- Tab panes -->
                        <div class="panel-body">
                          <div class="tab-content">
                            <div  class="tab-pane fade in active" id="tab-5">
                                <h2>Conocemos tu idea</h2>

                                <img class="alignright" src="assets/img/idea.png" alt="idea">

                                <p>Nos encanta comenzar a planificar tu proyecto a tu lado, nos convertimos en parte de tu equipo para conocer tu idea de negocio. <strong class="mk-text-dark text-bold">Antes de escribir una línea de código entendemos la factibilidad de tu negocio</strong>, con esto buscamos darte el valor agregado para que tu proyecto llegue tan lejos como quieras.</p>
                            </div>
                            <div class="tab-pane fade" id="tab-6">
                                <h2>Platificamos el concepto</h2>

                                <img class="alignright" src="assets/img/strategy.png" alt="estrategia">

                                <p>Nuestro equipo de ingenieros te ayuda a planificar tu proyecto, <strong class="mk-text-dark text-bold">Te entregamos los tiempos que nos llevara desarrollar tu idea</strong>, además de esto te ayudamos a orientar tu proyecto para que se realice al menor tiempo y costo posible para ti.</p>

                            </div>
                            <div  class="tab-pane fade" id="tab-7">
                                <h2>Desarrollamos</h2>

                                <img class="alignright" src="assets/img/coding.png" alt="desarrollamos">

                                <p>Contamos con un equipo de desarrollo que estará trabajando activamente en el desarrollo, <strong class="mk-text-dark text-bold">Mientras esto sucede podrás estar en contacto con alguien de nuestro equipo</strong>, nuestros desarrollos estan realizados con amplios estándares de calidad y haciendo uso de las últimas tecnologías.</p>
                            </div>
                            <div  class="tab-pane fade" id="tab-8">
                                <h2>¡Tenemos listo tu producto!</h2>

                                <img class="alignright" src="assets/img/checked.png" alt="producto-listo">

                                <p>Te entregamos tu producto terminado y desarrollado con gran calidad, <strong class="mk-text-dark text-bold">¡Nada de compromisos forzosos, no hay rentas mensuales, el código es 100% tuyo!</strong>, si una vez entregado tu producto tienes dudas o necesitas ayuda adicional, siempre nos encantara ayudarte.</p>

                              
                            </div>
                          </div>
                        </div>
                  </div>
                </div><!-- /.row -->
        </div><!-- /.vertical-tab -->


       <div class="promo-box dark-bg mt-80">
                <div class="promo-info">
                    <h2 class="white-text text-xs-center text-uppercase text-bold no-margin">Deja tus datos si prefieres que nosotros <span class="brand-color">TE LLÁMEMOS</span></h2>
                </div>
                <div class="promo-btn">
                     <a target="_blank" href="contacto" class="btn btn-lg waves-effect waves-light text-bold markdevs-b">
                     <!--<i class="material-icons left">attach_money</i>-->
                     ¡QUIERO QUE ME LLAMEN!</a>
                </div>
        </div>



    </div>
    

</section>


        <?php
include_once 'includes/footer.html';
// include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>
