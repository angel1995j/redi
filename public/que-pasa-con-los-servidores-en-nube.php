
<?php 
    $title = "¿Que pasa con los servidores en la nube?";
    $path = dirname(__DIR__);
    include_once 'includes/head.php';?>
 <body id="top" class="has-header-search">

       <?php
        include_once 'includes/header-page.html';
        include_once 'includes/menu.php';
        ?>

        <!--page title start-->
        <section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Nuestro Articulo</h2>
                        <ol class="breadcrumb">
                            <li><a href="#">Inicio</a></li>
                            <li><a href="#">Blog</a></li>
                         
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->
        

        <!-- blog section start -->
        <section class="blog-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                      <div class="posts-content single-post">

                        <article class="post-wrapper">

                          <header class="entry-header-wrapper clearfix">

                            <div class="author-thumb waves-effect waves-light">
                              <a href="#"><img src="assets/img/blog/author.jpg" alt="autor"></a>                
                            </div>
                            
                            <div class="entry-header">
                              <h2 class="entry-title">¿QUE PASA CON LOS SERVIDORES EN NUBE?</h2>

                              <div class="entry-meta">
                                  <ul class="list-inline">
                                      <li>
                                          <i class="fa fa-user"></i><a href="#">ADMIN</a>
                                      </li>

                                      <li>
                                          <i class="fa fa-clock-o"></i><a href="#">Dic 10, 2019</a>
                                      </li>

                                      <li>
                                          <i class="fa fa-heart-o"></i><a href="#"><span>265</span></a>
                                      </li>

                                      <li>
                                          <i class="fa fa-comment-o"></i><a href="#">0</a>
                                      </li>
                                  </ul>
                              </div><!-- .entry-meta -->
                            </div>

                          </header><!-- /.entry-header-wrapper -->

                          <div class="entry-header-wrapper clearfix">
                            <img src="assets/img/blog/blog3.jpg" class="img-responsive" alt="autor" >
                          </div><!-- .post-thumb -->


                          <div class="entry-content">


                            <p></p>

                            <p class="text-justify">El tema de los servidores en nube definitivamente es el top del momento, ¿Pero porque causan tanto revuelo?, ¿que los hace tan geniales como dicen?</p>

                            <h2>Son muchas cosas, pero primero vamos a empezar explicando un poco que son.</h2>

                            <p class="text-justify">Últimamente los volúmenes de datos se han vuelto más y más grandes lo cual se ha vuelto un descontrol para lo cual aun no estábamos preparado, y vaya con todo esto los servidores fisicos se volvian cada vez más robustos, aunado a la tendencia del internet de las cosas los sevidores fisicos no representan una gran ventaja sobre lo tradicional.</p>
                            
                            <p class="text-justify">Y es que un servidor fisico tiene muchos riesgos, supongamos que tu empresa aún cuenta con sevidores fisicos un dia en la tarde algo sucede un rayo cae y hace que todo colapse, quieres culpar a todo el mundo pero en realidad ha sido un fenomeno natural, misma situaciín para otros fenomenos natutales</p>
                            
                            <p class="text-justify">Es entonces cuando nuestra infrestructura fisica se mostraba muy vulnerada y asi lo es, hay que considerar el factor de que los sistemas cada vez se empujan más a ser multiplataforma, cosa que no sucedia hace algunos años.</p>
                            
                            <p class="text-justify">Todo esto llevo a la creación del internet en la nube o “cloud computing”, además de delegar la responsabilidad de administración a un tercero 100% especializado en el tema , ofreces una mayor seguridad a tu información</p>
                            
                            <p class="text-justify">Actualmente hay un inumerable numero de soluciones para cloud computing , sin embargo recomendamos hacer una evaluación tecnica para esto, existen opciones muy populares en el mundo como podrian ser Amazon web Services o Google Cloud, Rackspace entre muchas otras</p>

                            <p class="text-justify">Estas tres citadas anteriormente actuan bajo una politica que se esta haciendo tendencia de “paga solo lo que usas”, cosa que ha sido genial para potenciar proyectos con una escalabilidad proxima, este tipo de servicios cuentan además con centros de datos ubicados estrategicamente en varias partes del mundo lo cual suma un activo importante con grandes manejos de datos.</p>

                            <p>Si tienes dudas de cloud computing no olvides enviarnos un correo o dejarnos tu comentario..</p>

                            <p>¡Exito!</p>

                           
                            
                          </div><!-- .entry-content -->


                          <footer class="entry-footer">
                            <div class="post-tags">
                              <span class="tags-links">
                                <i class="fa fa-tags"></i><a href="#">Tecnología,,</a> <a href="#" rel="tag"></a>
                              </span>
                            </div> <!-- .post-tags -->

                            <ul class="list-inline right share-post">
                                <li><a href="#"><i class="fa fa-facebook"></i> <span>Compartir</span></a>
                                </li>                                
                                </li>
                                <li><a href="#"><i class="fa fa-google-plus"></i> <span>Más</span></a>
                                </li>
                            </ul>
                          </footer>

                        </article><!-- /.post-wrapper -->

                        <nav class="single-post-navigation" role="navigation">
                          <div class="row">
                            <!-- Previous Post -->
                            <div class="col-xs-6">
                              <div class="previous-post-link">
                                <a class="waves-effect waves-light" href="todo-lo-que-necesitas-saber-sobre-el-seo.php"><i class="fa fa-long-arrow-left"></i>Leer publicación anterior</a>
                              </div>
                            </div>

                            <!-- Next Post -->
                            <div class="col-xs-6">
                              <div class="next-post-link">
                                <a class="waves-effect waves-light" href="importancia-del-diseno-responsivo.php">Leer siguiente publicación<i class="fa fa-long-arrow-right"></i></a>
                              </div>
                            </div>
                          </div> <!-- .row -->
                        </nav>

                      </div><!-- /.posts-content -->
                    </div><!-- /.col-md-12 -->

                  </div><!-- /.row -->
            </div><!-- /.container -->
        </section>
        <!-- blog section end -->

        <?php
        include_once 'includes/footer.html';
        include_once 'includes/preloader.html';
        include_once 'includes/scripts.html';
         ?>
