<?php
$title = "Proyectos";
include_once 'includes/head.php'?>
    <body id="top" class="has-header-search">

       <?php
       include_once 'includes/header-page.html';
        include_once 'includes/menu.php';

        ?>
        <!--page title start-->
        <section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Nuestros Proyectos</h2>
                        <ol class="breadcrumb">
                            <li><a href="#">Inicio</a></li>
                            <li class="active">Proyectos</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->
        

        <section class="section-padding">
          <div class="container">
            <?php include_once 'includes/portfolio.html';?>

        </div>
        </section>

        <?php
include_once 'includes/footer.html';
include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>
