$(document).ready(function() {
  var pricePacket = 6990;

  $(".box").click(function() {
    pricePacket = $(this).data("price");
  });

  $(document).on("click keyup", ".mis-checkboxes, .box", function() {
    calcular(pricePacket);
  });

  $('a.smooth').on('click', function (e) {
    e.preventDefault();
    var $link = $(this);
    var anchor = $link.attr('href');
    $('html, body').stop().animate({
      scrollTop: $(anchor).offset().top
    }, 100);
  });

});

function calcular(pricePacket) {
  var tot = $("#total");
  tot.val(0);
  $(".mis-checkboxes,.mis-adicionales").each(function() {
    if ($(this).hasClass("mis-checkboxes")) {
      tot.val(
        ($(this).is(":checked")
          ? parseFloat($(this).attr("tu-attr-precio"))
          : 0) + parseFloat(tot.val())
      );
    } else {
      tot.val(
        parseFloat(tot.val()) +
          (isNaN(parseFloat($(this).val())) ? 0 : parseFloat($(this).val()))
      );
    }
  });

  tot.val(parseFloat(tot.val()) + parseFloat(pricePacket));
  var totalParts = parseFloat(tot.val())
    .toFixed(2)
    .split(".");

  tot.val(
    "$" +
      totalParts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
      "." +
      (totalParts.length > 1 ? totalParts[1] : "00")
  );
}

// Add smooth scrolling to all links
$("a").on("click", function(event) {
  // Make sure this.hash has a value before overriding default behavior
  if (this.hash !== "") {
    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
    $("html, body").animate(
      {
        scrollTop: $(hash).offset().top
      },
      800,
      function() {
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      }
    );
  } // End if
});
