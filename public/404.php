<?php
$title = "Agencia de desarrollo web";
include_once 'includes/head.php'?>
    <body>

        
        <section class="fullscreen-banner valign-wrapper error-wrapper-alt">
            <div class="valign-cell">
              <div class="container">
                <div class="text-center">
                    <h1 class="mb-30">404</h1>
                    <span class="error-sub">OOPS! PÁGINA NO ENCONTRADA</span>

                    <p>Lo sentimos, pero parece que no podemos encontrar la página que está buscando.</p>
                    
                    <a class="btn btn-lg waves-effect waves-light" href="/redi/public/">INICIO</a>
                </div>
              </div><!-- /.container -->
            </div><!-- /.valign-cell -->
        </section>



        <?php
            include_once 'includes/preloader.html';
            include_once 'includes/scripts.html';
        ?>

    </body>
  
</html>