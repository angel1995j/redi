<?php
$title = "Desarrollo de páginas web";
include_once 'includes/head.php';?>
    <body id="top" class="has-header-search">

       <?php
include_once 'includes/header-page.html';
include_once 'includes/menu.php';

?>
       <!--page title start-->
        <section class="page-title ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Servicios</h2>
                        <ol class="breadcrumb">
                            <li><a href="#">Inicio</a></li>
                            <li >Servicios</li>
                            <li class="active" >Páginas Web</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--page title end-->
        <section class=" section-padding">
            <div class="container">
              <div class="row">
                <div class="col-md-7">
                	<h2 class="font-40 mb-30 ">DESARROLLO DE PÁGINAS WEB</h2>
                	<p class="text-justify">En Markdevs realizamos cualquier tipo de sitio web utilizando los más amplios estándares de calidad, todos nuestros sitios web se realizan adaptables a móviles y con las mejores prácticas de diseño y desarrollo web.</p>

                	<ul class="list-icon mb-30">
                		<li><i class="material-icons">&#xE876;</i> Diseño personalizado.</li>
                		<li><i class="material-icons">&#xE876;</i> Adaptado a Móviles.</li>
                		<li><i class="material-icons">&#xE876;</i> Código optimizado.</li>
                        <li><i class="material-icons">&#xE876;</i> Optimización SEO.</li>
                	</ul>

                	<a href="contacto" class="m-auto btn btn-lg text-capitalize waves-effect waves-light markdevs">
                    <i class="material-icons left">headset_mic</i>
                    Contactar un agente</a>
                    <a target="_blank" href="/redi/public/cotizar" class="btn btn-lg waves-effect waves-light text-bold markdevs-b">
                     <i class="material-icons left">attach_money</i>
                     Cotizar en Linea</a>

                </div><!-- /.col-md-7 -->

                <div class="col-md-5 mt-sm-30">
					         <img src="assets/img/services/paginas.jpeg" alt="paginas" class="img-responsive">
                </div><!-- /.col-md-5 -->
              </div><!-- /.row -->
            </div><!-- /.container -->
        </section>


<?php
include_once 'includes/footer.html';
include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>



<?php
include_once 'includes/footer.html';
include_once 'includes/preloader.html';
include_once 'includes/scripts.html';
?>
